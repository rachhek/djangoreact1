
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
urlpatterns = [
    path('', include('frontend.urls')),
    path('', include('leads.urls')),
    # path('', include('books.urls')),
    path('', include('accounts.urls')),
    path('', include('coursebooks.urls')),
    path('', include('music.urls')),
    path('admin/', admin.site.urls)
]
