import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { transitions, positions, Provider as AlertProvider } from "react-alert";
import AlertTemplate from "react-alert-template-basic";

import {
  HashRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";

import Header from "./layouts/Header";
import Dashboard from "./leads/Dashboard";
import BooksDashboard from "./books/BooksDashboard";

import Alerts from "./layouts/Alerts";

import Login from "./accounts/Login";
import Register from "./accounts/Register";
import RequestPassword from "./accounts/RequestPassword";

import PrivateRoute from "./common/PrivateRoute";

import { Provider } from "react-redux";
import store from "../store";

import { loadUser } from "../actions/auth";

import MusicDashboard from "./music/MusicDashboard";

//Alert Options
const alertOptions = {
  timeout: 3000,
  position: "top center"
};

class App extends Component {
  componentDidMount() {
    console.log("component mounted");
    store.dispatch(loadUser());
  }

  render() {
    return (
      <Provider store={store}>
        <AlertProvider template={AlertTemplate} {...alertOptions}>
          <Router>
            <Fragment>
              <Header />
              <Alerts />
              <div className="container">
                <Switch>
                  <PrivateRoute exact path="/" component={Dashboard} />
                  <PrivateRoute
                    exact
                    path="/books"
                    component={BooksDashboard}
                  />

                  <Route exact path="/login" component={Login} />
                  <Route exact path="/register" component={Register} />
                  <Route exact path="/music" component={MusicDashboard} />
                  <Route exact path="/pwreset" component={RequestPassword} />
                </Switch>
              </div>
            </Fragment>
          </Router>
        </AlertProvider>
      </Provider>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("app"));
