import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getBooks, addBook, deleteBook, updateBook } from "../../actions/books";
import BootstrapTable from "react-bootstrap-table-next";
import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";
import paginationFactory from "react-bootstrap-table2-paginator";
import cellEditFactory from "react-bootstrap-table2-editor";
import bootstrapTable from "react-bootstrap-table-next/lib/src/bootstrap-table";

export class Books extends Component {
  static propTypes = {
    books: PropTypes.array.isRequired,
    getBooks: PropTypes.func.isRequired,
    deleteBook: PropTypes.func.isRequired,
    updateBook: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.getBooks();
  }

  render() {
    const data = this.props.books;

    const columns = [
      {
        dataField: "name",
        text: "Book Name"
      },
      {
        dataField: "author",
        text: "Author Name",
        sort: true
      },
      {}
    ];

    const cellEdit = cellEditFactory({
      mode: "click",
      afterSaveCell: (oldValue, newValue, row, column) => {
        console.log("updated");
        this.props.updateBook(row);
      }
    });

    return (
      <Fragment>
        <h2>Books</h2>
        <BootstrapTable
          keyField="id"
          data={data}
          columns={columns}
          striped
          pagination={paginationFactory()}
          cellEdit={cellEdit}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  books: state.books.books
});

export default connect(
  mapStateToProps,
  { getBooks, deleteBook, updateBook }
)(Books);
