import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { addBook } from "../../actions/books";

export class BookForm extends Component {
  state = {
    name: "",
    author: ""
  };

  static propTypes = {
    addBook: PropTypes.func.isRequired
  };

  onChange = e => this.setState({ [e.target.name]: e.target.value });

  onSubmit = e => {
    e.preventDefault();
    const { name, author } = this.state;
    const book = { name, author };
    this.props.addBook(book);
    this.setState({
      name: "",
      author: ""
    });
  };

  render() {
    const { name, author } = this.state;
    return (
      <div className="card card-body mt-4 mb-4">
        <h2>Add Book</h2>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Name</label>
            <input
              className="form-control"
              type="text"
              name="name"
              onChange={this.onChange}
              value={name}
            />
          </div>
          <div className="form-group">
            <label>Author</label>
            <input
              className="form-control"
              type="text"
              name="author"
              onChange={this.onChange}
              value={author}
            />
          </div>
          <div className="form-group">
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </div>
        </form>
      </div>
    );
  }
}
export default connect(
  null,
  { addBook }
)(BookForm);
