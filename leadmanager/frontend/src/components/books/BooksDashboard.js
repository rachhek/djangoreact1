import React, { Fragment } from "react";
import BookForm from "./BookForm";
import Books from "./Books";

export default function Dashboard() {
  return (
    <Fragment>
      <BookForm />
      <Books />
    </Fragment>
  );
}
