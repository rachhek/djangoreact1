import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

export class RequestPassword extends Component {
  state = {
    email: ""
  };

  static propTypes = {
    // prop: PropTypes
  };

  onSubmit = e => {
    e.preventDefault();

    const { email } = this.state;
    this.props.createMessage({
      passwordNotMatch:
        "Password Reset Link sent to the registered email address"
    });
    // if (password.toString() != password2.toString()) {
    //   this.props.createMessage({ passwordNotMatch: "Passwords do not match" });
    // } else {
    //   const newUser = {
    //     username,
    //     password,
    //     email,
    //     organization
    //   };
    this.props.register(newUser);
    // }
  };

  onChange = e => this.setState({ [e.target.name]: [e.target.value] });

  render() {
    const { email } = this.state;
    return (
      <div className="col-md-6 m-auto">
        <div className="card card-body mt-5">
          <h2 className="text-center">Request Password</h2>
          <form onSubmit={this.onSubmit}>
            <div className="form-group">
              <label>Email</label>
              <input
                type="email"
                className="form-control"
                name="email"
                onChange={this.onChange}
                value={email}
              />
            </div>
            <div className="form-group">
              <button type="submit" className="btn btn-primary">
                Send Password link
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RequestPassword);
