import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { addArtist, getArtists } from "../../actions/music";

export class Artistform extends Component {
  state = {
    name: ""
  };

  static propTypes = {
    addArtist: PropTypes.func.isRequired
  };

  onChange = e => this.setState({ [e.target.name]: e.target.value });

  onSubmit = e => {
    e.preventDefault();
    const { name } = this.state;
    const artist = { name };
    this.props.addArtist(artist);
    this.setState({
      name: ""
    });
  };

  render() {
    const { name } = this.state;
    return (
      <div className="card card-body mt-4 mb-4">
        <h2>Add Artist</h2>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Name</label>
            <input
              className="form-control"
              type="text"
              name="name"
              onChange={this.onChange}
              value={name}
            />
          </div>
          <div className="form-group">
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = { addArtist };

//connect is to connect react to redux
export default connect(
  null,
  mapDispatchToProps
)(Artistform);
