import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getArtists } from "../../actions/music";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";

export class Artistlist extends Component {
  static propTypes = {
    artists: PropTypes.array.isRequired,
    getArtists: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.getArtists();
  }

  render() {
    const data = this.props.artists;

    const columns = [
      {
        dataField: "id",
        text: "ID"
      },
      {
        dataField: "name",
        text: "Artist Name",
        sort: true
      }
    ];
    const artistlist = this.props.artists;
    return (
      <Fragment>
        <h2>Artist</h2>
        <BootstrapTable
          keyField="id"
          data={data}
          columns={columns}
          striped
          pagination={paginationFactory()}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  artists: state.music.artists
});

const mapDispatchToProps = { getArtists };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Artistlist);
