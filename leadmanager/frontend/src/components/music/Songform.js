import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { addSong, getSongs } from "../../actions/music";

export class Songform extends Component {
  state = {
    name: "",
    artist: ""
  };

  static propTypes = {
    addSong: PropTypes.func.isRequired,
    artists: PropTypes.array.isRequired
  };

  onChange = e => this.setState({ [e.target.name]: e.target.value });

  onSubmit = e => {
    e.preventDefault();
    const { name, artist } = this.state;
    const song = { name, artist };
    this.props.addSong(song);
    this.setState({
      name: "",
      artist: ""
    });
  };

  render() {
    const { name, artist } = this.state;
    let optionItems = this.props.artists.map(artist => (
      <option key={artist.id} value={artist.id}>
        {artist.name}
      </option>
    ));

    return (
      <div className="card card-body mt-4 mb-4">
        <h2>Add Song</h2>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Name</label>
            <input
              className="form-control"
              type="text"
              name="name"
              onChange={this.onChange}
              value={name}
            />
          </div>
          <div className="form-group">
            <label>Artist</label>
            <select
              className="form-control"
              name="artist"
              onChange={this.onChange}
              value={artist}
            >
              {optionItems}
            </select>
            {/* <input
              className="form-control"
              type="dr"
              name="artist"
              onChange={this.onChange}
              value={artist}
            /> */}
          </div>
          <div className="form-group">
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  artists: state.music.artists
});

const mapDispatchToProps = { addSong };

//connect is to connect react to redux
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Songform);
