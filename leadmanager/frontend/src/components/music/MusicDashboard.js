import React, { Fragment } from "react";
import Artistform from "./Artistform";
import Artistlist from "./Artistlist";
import Songform from "./Songform";
import Songlist from "./Songlist";
import { Container, Row, Col } from "react-bootstrap";
export default function MusicDashboard() {
  return (
    <Fragment>
      <Container>
        <Row>
          <Col>
            <Artistform />
            <Artistlist />
          </Col>
          <Col />
          <Col>
            <Songform />
            <Songlist />
          </Col>
        </Row>
      </Container>
    </Fragment>
  );
}
