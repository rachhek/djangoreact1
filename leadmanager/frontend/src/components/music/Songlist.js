import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getSongs } from "../../actions/music";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";

export class Songlist extends Component {
  static propTypes = {
    songs: PropTypes.array.isRequired,
    getSongs: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.getSongs();
  }

  render() {
    const data = this.props.songs;

    const columns = [
      {
        dataField: "id",
        text: "ID"
      },
      {
        dataField: "name",
        text: "Song Name",
        sort: true
      },
      {
        dataField: "artist",
        text: "Artist Name"
      }
    ];
    const artistlist = this.props.artists;
    return (
      <Fragment>
        <h2>Songs</h2>
        <BootstrapTable
          keyField="id"
          data={data}
          columns={columns}
          striped
          pagination={paginationFactory()}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  songs: state.music.songs
});

const mapDispatchToProps = { getSongs };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Songlist);
