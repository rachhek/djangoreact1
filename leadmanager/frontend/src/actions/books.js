import axios from "axios";
import { createMessage, returnErrors } from "./messages";

import { ADD_BOOK, GET_BOOK, DELETE_BOOK, UPDATE_BOOK } from "./types";
import { tokenConfig } from "./auth";

export const addBook = book => (dispatch, getState) => {
  axios.post("/api/books/", book, tokenConfig(getState)).then(res => {
    dispatch(createMessage({ addBook: "Book added" }));
    dispatch({
      type: ADD_BOOK,
      payload: res.data
    });
  });
};

export const updateBook = book => (dispatch, getState) => {
  axios
    .put(`/api/books/${book.id}/`, book, tokenConfig(getState))
    .then(res => {
      dispatch(
        createMessage({
          editLead: "Book edited"
        })
      );
      dispatch({
        type: UPDATE_BOOK,
        payload: {
          name: book.name,
          author: book.author
        }
      });
    })
    .catch(err => console.log(err));
};

export const getBooks = () => (dispatch, getState) => {
  axios
    .get("/api/books/", tokenConfig(getState))
    .then(res => {
      dispatch({
        type: GET_BOOK,
        payload: res.data
      });
    })
    .catch(err =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};

//Delete LEAD

export const deleteBook = id => (dispatch, getState) => {
  axios
    .delete(`/api/books/${id}/`, tokenConfig(getState))
    .then(res => {
      dispatch(
        createMessage({
          deleteLead: "Book Deleted"
        })
      );
      dispatch({
        type: DELETE_BOOK,
        payload: id
      });
    })
    .catch(err => console.log(err));
};
