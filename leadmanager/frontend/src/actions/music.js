import axios from "axios";
import { createMessage, returnErrors } from "./messages";

import {
  ADD_ARTIST,
  GET_ARTIST,
  ADD_SONG,
  GET_SONG,
  GET_BOOK,
  DELETE_BOOK,
  UPDATE_BOOK
} from "./types";
import { tokenConfig } from "./auth";

export const addArtist = artist => (dispatch, getState) => {
  axios.post("/api/music/artist/", artist).then(res => {
    dispatch(createMessage({ addArtist: "Artist Added" }));
    dispatch({
      type: ADD_ARTIST,
      payload: res.data
    });
  });
};

export const addSong = song => (dispatch, getState) => {
  axios.post("/api/music/songs/", song).then(res => {
    dispatch(createMessage({ addSong: "Song Added" }));
    dispatch({
      type: ADD_SONG,
      payload: res.data
    });
  });
};

// export const updateBook = book => (dispatch, getState) => {
//   axios
//     .put(`/api/books/${book.id}/`, book)
//     .then(res => {
//       dispatch(
//         createMessage({
//           editLead: "Book edited"
//         })
//       );
//       dispatch({
//         type: UPDATE_BOOK,
//         payload: {
//           name: book.name,
//           author: book.author
//         }
//       });
//     })
//     .catch(err => console.log(err));
// };

export const getArtists = () => (dispatch, getState) => {
  axios
    .get("/api/music/artist/")
    .then(res => {
      dispatch({
        type: GET_ARTIST,
        payload: res.data
      });
    })
    .catch(err =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};

export const getSongs = () => (dispatch, getState) => {
  axios
    .get("/api/music/songs/")
    .then(res => {
      dispatch({
        type: GET_SONG,
        payload: res.data
      });
    })
    .catch(err =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};

//Delete LEAD

// export const deleteBook = id => (dispatch, getState) => {
//   axios
//     .delete(`/api/books/${id}/`, tokenConfig(getState))
//     .then(res => {
//       dispatch(
//         createMessage({
//           deleteLead: "Book Deleted"
//         })
//       );
//       dispatch({
//         type: DELETE_BOOK,
//         payload: id
//       });
//     })
//     .catch(err => console.log(err));
// };
