import {
  GET_BOOK,
  DELETE_BOOK,
  ADD_BOOK,
  UPDATE_BOOK
} from "../actions/types.js";

const initialState = {
  books: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_BOOK:
      return {
        ...state,
        books: action.payload
      };

    case DELETE_BOOK:
      return {
        ...state,
        books: state.books.filter(book => book.id != action.payload)
      };

    case UPDATE_BOOK:
      return {
        ...state,
        books: state.books.filter(book => book.id != action.payload)
      };

    case ADD_BOOK:
      return {
        ...state,
        books: [...state.books, action.payload]
      };

    default:
      return state;
  }
}
