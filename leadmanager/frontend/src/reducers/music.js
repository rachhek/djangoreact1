import {
  GET_ARTIST,
  GET_SONG,
  ADD_SONG,
  DELETE_BOOK,
  ADD_ARTIST,
  UPDATE_BOOK
} from "../actions/types.js";

const initialState = {
  artists: [],
  songs: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_ARTIST:
      return {
        ...state,
        artists: action.payload
      };

    case GET_SONG:
      return {
        ...state,
        songs: action.payload
      };

    case ADD_SONG:
      return {
        ...state,
        songs: [...state.songs, action.payload]
      };

    // case DELETE_BOOK:
    //   return {
    //     ...state,
    //     books: state.books.filter(book => book.id != action.payload)
    //   };

    // case UPDATE_BOOK:
    //   return {
    //     ...state,
    //     books: state.books.filter(book => book.id != action.payload)
    //   };

    case ADD_ARTIST:
      return {
        ...state,
        artists: [...state.artists, action.payload]
      };

    default:
      return state;
  }
}
