import { combineReducers } from "redux";
import leads from "./leads";
import books from "./books";
import errors from "./errors";
import messages from "./messages";
import music from "./music";
import auth from "./auth";

export default combineReducers({
  leads,
  errors,
  messages,
  auth,
  books,
  music
});
