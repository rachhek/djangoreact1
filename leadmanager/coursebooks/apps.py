from django.apps import AppConfig


class CoursebooksConfig(AppConfig):
    name = 'coursebooks'
