from django.db import models

# Create your models here.


class Course(models.Model):
    name = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)


class Books(models.Model):
    name = models.CharField(max_length=300)
    created_at = models.DateTimeField(auto_now_add=300)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
