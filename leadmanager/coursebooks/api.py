from coursebooks.models import Course, Books
from coursebooks.serializers import CourseSerializer, BooksSerializer
from rest_framework import viewsets, permissions

# django filter backend to filter the api
from django_filters.rest_framework import DjangoFilterBackend


class CourseViewSet(viewsets.ModelViewSet):

    queryset = Course.objects.all()
    serializer_class = CourseSerializer


class BooksViewSet(viewsets.ModelViewSet):
    filter_backends = (DjangoFilterBackend,)

    # 127.0.0.1:8000/api/books?course__id=2
    # Course__ID is a foreign key
    filterset_fields = ('name', 'course__id', 'course__name',)

    queryset = Books.objects.all()
    serializer_class = BooksSerializer
