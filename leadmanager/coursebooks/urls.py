from rest_framework import routers
from .api import CourseViewSet, BooksViewSet

router = routers.DefaultRouter()
router.register('api/courses', CourseViewSet, 'courses')
router.register('api/books', BooksViewSet, 'books')

urlpatterns = router.urls
