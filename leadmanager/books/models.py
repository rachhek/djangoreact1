from django.db import models
# from django.contrib.auth.models import User
from django.conf import settings
# Create your models here.


class Book(models.Model):
    name = models.CharField(max_length=100)
    author = models.CharField(max_length=200)
    added_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="books", on_delete=models.CASCADE, null=True)
    added_at = models.DateTimeField(auto_now_add=True)
