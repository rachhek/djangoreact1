from rest_framework import viewsets, permissions
from books.models import Book
from books.serializer import BookSerializer


class BookViewSet(viewsets.ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated
    ]

    def get_queryset(self):
        return self.request.user.books.all()

    def perform_create(self, serializer):
        serializer.save(added_by=self.request.user)

    serializer_class = BookSerializer
