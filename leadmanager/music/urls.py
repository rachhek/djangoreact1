from rest_framework import routers
from .api import ArtistViewSet, SongViewSet

router = routers.DefaultRouter()
router.register('api/music/artist', ArtistViewSet, 'artists')
router.register('api/music/songs', SongViewSet, 'songs')

urlpatterns = router.urls
