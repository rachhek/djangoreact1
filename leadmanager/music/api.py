from music.models import Song, Artist
from music.serializers import SongSerializer, ArtistSerializer
from rest_framework import viewsets, permissions

# django filter backend to filter the api
from django_filters.rest_framework import DjangoFilterBackend


class SongViewSet(viewsets.ModelViewSet):
    print("hi")
    filter_backends = (DjangoFilterBackend,)

    # 127.0.0.1:8000/api/books?course__id=2
    # Course__ID is a foreign key
    filterset_fields = ('name', 'artist__id', 'artist__name',)
    queryset = Song.objects.all()
    serializer_class = SongSerializer


class ArtistViewSet(viewsets.ModelViewSet):
    filter_backends = (DjangoFilterBackend,)

    # 127.0.0.1:8000/api/books?course__id=2
    # Course__ID is a foreign key
    filterset_fields = ('name',)

    queryset = Artist.objects.all()
    serializer_class = ArtistSerializer
