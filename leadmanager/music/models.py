from django.db import models
from django.utils.translation import gettext as _
# Create your models here.


class Artist(models.Model):
    name = models.CharField(_("name"), max_length=50)
    #name = models.CharField(_(""), max_length=50)
    created_at = models.DateTimeField(
        _("created_at"), auto_now_add=True)

    class Meta:
        verbose_name = _("Artist")
        verbose_name_plural = _("Artists")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Artist_detail", kwargs={"pk": self.pk})


class Song(models.Model):
    name = models.CharField(_("Song"), max_length=50)
    artist = models.ForeignKey("music.Artist", verbose_name=_(
        "artist"), related_name="songs", on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Song")
        verbose_name_plural = _("Songs")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Song_detail", kwargs={"pk": self.pk})
