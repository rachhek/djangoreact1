from django.shortcuts import render

# Create your views here.
from rest_auth.registration.views import RegisterView
from .models import User


class CustomRegisterView(RegisterView):
    queryset = User.objects.all()
