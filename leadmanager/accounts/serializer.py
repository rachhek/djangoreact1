from rest_framework import serializers
# from django.contrib.auth.models import User
from django.conf import settings
from django.contrib.auth import authenticate
from .models import User
# User Serializer


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'fullname', 'email', 'organization')

# Register Serializer


class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'fullname',
                  'email', 'password', 'organization')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):

        user = User.objects.create_user(
            validated_data['email'], validated_data['fullname'], validated_data['organization'], validated_data['password'])

        return user


# Login Serializer
# didnt do ModelSerializer because we are not dealing with creating a Model but just doing login check
class LoginSerializer(serializers.Serializer):
    email = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        user = authenticate(**data)
        if user and user.is_active:
            return user
        raise serializers.ValidationError("Incorrect Credentials")

# from rest_auth.registration.serializers import RegisterSerializer
# from rest_framework import serializers
# from accounts.models import User


# class CustomRegisterSerializer(RegisterSerializer):

#     email = serializers.EmailField(required=True)
#     password1 = serializers.CharField(write_only=True)
#     fullname = serializers.CharField(required=True)
#     organization = serializers.DateField(required=True)

#     def get_cleaned_data(self):
#         super(CustomRegisterSerializer, self).get_cleaned_data()

#         return {
#             'password1': self.validated_data.get('password1', ''),
#             'email': self.validated_data.get('email', ''),
#             'fullname': self.validated_data.get('fullname', ''),
#             'organization': self.validated_data.get('organization', ''),
#         }


# class CustomUserDetailsSerializer(serializers.ModelSerializer):

#     class Meta:
#         model = User
#         fields = ('email', 'fullname', 'organization')
#         read_only_fields = ('email',)
