from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager


class UserManager(BaseUserManager):
    use_in_migrations = True

    def create_user(self, email, fullname, organization, password=None):
        user = self.model(
            username=email,
            email=self.normalize_email(email),
            organization=organization,
            fullname=fullname,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_staffuser(self, email, fullname, organization, password):
        user = self.create_user(
            email,
            password=password,
            organization=organization,
            fullname=fullname,
        )
        user.staff = True
        user.save(using=self._db)
        return user

    def create_superuser(self, email, fullname, organization, password):
        user = self.create_user(
            email,
            password=password,
            organization=organization,
            fullname=fullname,
        )
        user.staff = True
        user.admin = True
        user.save(using=self._db)
        return user


class User(AbstractUser):
    email = models.EmailField(max_length=254, unique=True)
    fullname = models.CharField(max_length=100)
    organization = models.CharField(max_length=50)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['fullname', 'organization']

    objects = UserManager()

    def __str__(self):
        return self.email
