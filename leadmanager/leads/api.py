from leads.models import Lead
from rest_framework import viewsets, permissions
from leads.serializers import LeadSerializer

from accounts.permissions import HasGroupPermission
from django.contrib.auth.models import Group
from drf_roles.mixins import RoleViewSetMixin
# Lead ViewSet


class LeadViewSet(RoleViewSetMixin, viewsets.ModelViewSet):

    permission_classes = [HasGroupPermission, permissions.IsAuthenticated]
    permission_groups = {
        'create': ['Developers'],  # Developers can POST
        # Designers and Developers can PATCH
        'partial_update': ['Designers', 'OverviewAdmins'],
        # retrieve can be accessed without credentials (GET 'site.com/api/foo/1')
        'retrieve': ['_Public'],
        'list': ['OverviewAdmins'],
        'destroy': ['OverviewAdmins']
    }

    def get_queryset(self):
        return self.request.user.leads.all()

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    serializer_class = LeadSerializer
